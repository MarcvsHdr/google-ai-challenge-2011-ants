Fall 2011 Google AI Challenge ACS @ UPB Bot Submission
======================================================

AUTHORS:

  Adrian Scoica (adrian.scoica@gmail.com)
  Valentin Dobrota (valydo@gmail.com)
  Victor Carbune (victor.carbune@gmail.com)
  Claudia Cardei (claudia.cardei@gmail.com)

ABOUT:

The Ant AI Bot was written in the Fall of 2011 for the purpose of competing
in the Google AI Challenge competition[0], where our team ranked 63 among 7897
teams from universities and independent teams from across the world.

REQUIREMENTS:

To run the AI Bot, you need to download the C++ source code from this
repository, and to compile it. We have so far tested the engine on 32bit and
64bit Debian-based Linux distributions only (eg. Debian, Ubuntu, and Linux Mint).

In Linux, run the following command with no parameters in the root directory
of the project in order to compile the binary:

> make

The engine is designed to run with the official competition staging and
visualization tools[1], and follows the communication protocol specified in
the official User Guide.

RUNNING:

After you've compiled the bot, an executable file called "MyBot" will be
created in the tools/ directory of the project. You can run the bot as follows:

(A) interactively (using a live visualization Java app);

> cd tools && ./play_one_game_live.sh

(B) in the background (no visualization, gameplay saved to logs):

> cd tools && ./play_one_game.sh

For more options, such as setting match modes and/or switching the players,
please refer to the scripts in the tools/ directory, or to the official AI
Challenge web page[0][1].

A video tutorial on the compiling and running engine is available at[2].

REFERENCES

[0] - http://ants.aichallenge.org/
[1] - http://ants.aichallenge.org/using_the_tools.php
[2] - https://www.youtube.com/watch?v=ixMp_TQDyzg
