#ifndef SQUARE_H_
#define SQUARE_H_

#include <vector>

#include "constants.h"

/**  Struct for representing a square in the grid. */
struct Square
{
  bool isVisible;
  bool isEnemyVisible;
  /* An accessible location is NOT water and WAS visible at LEAST once. */
  bool isAccessible;
  bool isWater;
  bool isHill;
  bool isFood;

  /* Whether the ant on the hill was just born. The current square MUST be a
   * our own hill. */
  bool isNewborn;

  /* Use to count whether the food on this square was gathered or not. */
  char previousFoodGathered;

  /** The player number of the ant in this square.
   *  0 == ME
   * -1 == No ant in this square
   *  * == other players
   */
  int ant; // char
  int nextTurnAnt; // char

  /** Index of the ant located on this Square in the myAnts array. */
  int antIndex;

  /** If a friendly ant moved here during this turn. */
  bool antMovedHere;

  /** The player number of the hill in this square.
   *  0 == ME
   * -1 == No hill in this square
   *  * == other players
   */
  int hillPlayer;

  /** Temporary distance. Only makes sense between successive BFS calls. */
  int tempDistance;

  /* The number of enemies that attack the square after t turns. */
  int danger[2];
  /* The number of friendlies that defend the square after t turns. */
  int defense[2];
  /* The minimum number of turns in which a friendly can occupy the square. */
  int ownClosest;
  int ownClosestFeederCandidate;
  /* The minimum number of turns in which an enemy can occupy the square. */
  int enemyClosest;
  /* The minimum number of turns in which a food situated on the square can be
   * gathered by a friendly. */
  int gatherTime;
  /* The minimum number of turns in which a food situated on the square can be
   * gathered by an enemy. */
  int enemyGatherTime;
  /* The minimum number of turns in which a friendly can occupy a square now
   * situated on the frontier */
  int frontierProximity;
  int virginFrontierProximity;
  /* The distance to the closest enemy hill. */
  int enemyHillProximity;
  /* The distance to the closest friendly ant who is an attacker. */
  int attackerProximity;
  /* The distance to the closest friendly hill. */
  int myHillProximity;
  /* The distance to the closest square where food can be gathered. */
  int closestGather;
  /* The closest friendly hill as index in myHills. */
  int closestOwnHill;
  /* The minimum defense for all future enemy ant positions that attack square. */
  int blow;
  /* The location of the attacked ant in case of ANT_ATTACKER. */
  Location attackedAntLocation;
  /* The location of the attacked hill in case of ATTACKER. */
  Location attackedHillLocation;    
  /* The location of the defended ant in case of HILL_DEFENDER. */ 
  Location defendedAntLocation;
  /* The location of the food in case of FEEDER. */
  Location feedingLocation;
  /* The largest group of ants attacking this location at t+1. */
  int largestAttackingGroup;

  /* The group size of the ant here. Makes sense only if .ant != -1. */
  int groupSize;

  /* The role of any ant that might be in this position. */
  Role role;

  /* The ant in this square is trying to move. */
  bool isMoving;

  double antAttackingScore[NUMBER_DIRECTIONS + 1];
  double antFeedingScore[NUMBER_DIRECTIONS + 1];

  /** Vector of final move scores for sorting moves vector, and unsorted 
   * vector of preferred moves from most to least desirable which will be
   * sorted from most to least desirable by the moving function in the bot.
   */
  double moveScore[NUMBER_DIRECTIONS + 1];
  std::vector<int> moves;

  /* Dead ants for current square. */
  std::vector<int> deadAnts;

  Square() :
      isVisible(false),
      isAccessible(false),
      isWater(false),
      isHill(false),
      isFood(false),
      isNewborn(true),
      previousFoodGathered(0),
      ant(-1),
      nextTurnAnt(-1),
      antIndex(-1),
      antMovedHere(false),
      hillPlayer(-1),
      ownClosest(MAXIMUM_ROAD),
      enemyClosest(MAXIMUM_ROAD),
      gatherTime(MAXIMUM_ROAD),
      enemyGatherTime(MAXIMUM_ROAD),
      frontierProximity(MAXIMUM_ROAD),
      virginFrontierProximity(MAXIMUM_ROAD),
      enemyHillProximity(MAXIMUM_ROAD),
      attackerProximity(MAXIMUM_ROAD),
      myHillProximity(MAXIMUM_ROAD),
      closestGather(MAXIMUM_ROAD),
      closestOwnHill(-1),
      blow(MAXIMUM_ROAD),
      attackedAntLocation(NOWHERE),        
      attackedHillLocation(NOWHERE),
      defendedAntLocation(NOWHERE),
      groupSize(0),
      largestAttackingGroup(0),
      isMoving(false),
      role(NOROLE)
  {
    danger[0] = danger[1] = defense[0] = defense[1] = 0;
  }

  /** Resets the information for the square except water and accesibility 
   * information. */
  void reset()
  {
    isVisible = false;
    isHill = false;
    isFood = false;
    isNewborn = true;
    previousFoodGathered = 0;
    ant = -1;
    nextTurnAnt = -1;
    antIndex = -1;
    antMovedHere = false;
    hillPlayer = -1;
    role = NOROLE;
    danger[0] = danger[1] = 0;
    defense[0] = defense[1] = 0;
    ownClosest = MAXIMUM_ROAD;
    enemyClosest = MAXIMUM_ROAD;
    gatherTime = MAXIMUM_ROAD;
    enemyGatherTime = MAXIMUM_ROAD;
    frontierProximity = MAXIMUM_ROAD;
    virginFrontierProximity = MAXIMUM_ROAD;
    enemyHillProximity = MAXIMUM_ROAD;
    attackerProximity = MAXIMUM_ROAD;
    myHillProximity = MAXIMUM_ROAD;
    closestGather = MAXIMUM_ROAD;
    closestOwnHill = -1;
    blow = MAXIMUM_ROAD;
    attackedAntLocation = NOWHERE;
    attackedHillLocation = NOWHERE;
    defendedAntLocation = NOWHERE;
    groupSize = 0;
    largestAttackingGroup = 0;
    deadAnts.clear();
    isMoving = false;
    moves.clear();
  }
};

#endif
