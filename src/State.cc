#include <cstdlib>
#include <iomanip>

#include "State.h"

/* Resets all non-water squares to land and clears the bots ant vector. */
void State::reset()
{
  inputMyAnts.clear();
  inputMyHills.clear();

  inputEnemyHills.clear();
  enemyAnts.clear();

  previousFood.clear();
  for (int i = 0; i < food.size(); i++) {
    previousFood.push_back(food[i]);
  }

  food.clear();

  for(int row = 0; row < gparam::mapRows; row++)
    for(int col = 0; col < gparam::mapColumns; col++)
      if(!grid[row][col].isWater)
        grid[row][col].reset();
};

/* Outputs move information to the engine. */
void State::makeMove(const Location loc, int direction)
{
  /* Operate the move and write it down. */
  Location newLoc = MOVE_DIRECTION(loc, direction);
  GRID_SQUARE(newLoc).antMovedHere = true;
  if (GRID_SQUARE(loc).ant == 0) {
    /* This should always be true. */
    myAnts[GRID_SQUARE(loc).antIndex].nextPosition = newLoc;
  } else {
    LOG(LOG_ERR, "Why are you trying to move an INVISIBLE ant?");
  }

  if ( GRID_SQUARE(newLoc).nextTurnAnt != -1)
    LOG(LOG_ERR, "Why are you trying to move two ants in the SAME square?");

  /* If you're just standing ground, don't print out anything, just silently
   * make a mark. */
  if (direction == NODIRECTION) {
    GRID_SQUARE(loc).nextTurnAnt = GRID_SQUARE(loc).ant;
    LOG (LOG_INF, "w " << GET_ROW(loc) << " " << GET_COLUMN(loc) << " " <<
        DIRECTION_LETTER[direction]);
  } else {
    GRID_SQUARE(newLoc).nextTurnAnt = GRID_SQUARE(newLoc).ant;
    std::cout << "o " << GET_ROW(loc) << " " << GET_COLUMN(loc) << " " <<
        DIRECTION_LETTER[direction] << std::endl;
    LOG (LOG_INF, "o " << GET_ROW(loc) << " " << GET_COLUMN(loc) << " " <<
        DIRECTION_LETTER[direction]);
  }
}

/* Returns the square of Euclid distance between two locations with the edges
   wrapped. */
double State::distance(const Location loc1, const Location loc2)
{
  int d11 = GET_ROW(loc1) - GET_ROW(loc2);
  int d1 = ABS(d11);
  int d22 = GET_COLUMN(loc1) - GET_COLUMN(loc2);
  int d2 = ABS(d22);
  int dr = MIN(d1, gparam::mapRows - d1);
  int dc = MIN(d2, gparam::mapColumns - d2);
  return dr*dr + dc*dc;
};

/* This is the output function for a state. It will add a char map
   representation of the state to the output stream passed to it.

   For example, you might call "cout << state << endl;" */
std::ostream& operator<<(std::ostream &os, const State &state)
{
  int antnumber = 0;
  for(int row = 0; row < gparam::mapRows; row++)
  {
    for(int col = 0; col < gparam::mapColumns; col++)
    {
      os.width(2);
      if(state.grid[row][col].isWater)
        os << '%';
      else if(state.grid[row][col].isFood)
        os << '*';
      else if(state.grid[row][col].isHill)
        os << (char)('A' + state.grid[row][col].hillPlayer);
      else if(state.grid[row][col].ant >= 0) {
        if (state.grid[row][col].ant == 0) {
          os.width(1);
          os << ROLE_SYMBOL[state.grid[row][col].role];
          os.width(1);
          os << ++antnumber;
        } else {
          os << (char)('a' + state.grid[row][col].ant);
        }
      }
      else if(state.grid[row][col].isVisible)
        os << '.';
      else
        os << '?';
    }
    os << std::endl;
  }

  antnumber = 0;
  for (int row = 0; row < gparam::mapRows; row++) {
    for (int col = 0; col < gparam::mapColumns; col++) {
      if (state.grid[row][col].ant == 0) {
        os << "Ant " << ++antnumber << "(" <<
            ROLE_SYMBOL[state.grid[row][col].role] << "): ";
        for (int dir = 0; dir < state.grid[row][col].moves.size(); ++dir) {
          os << DIRECTION_LETTER[state.grid[row][col].moves[dir]] << ":"
             << state.grid[row][col].moveScore[state.grid[row][col].moves[dir]]
             << " ";
        }
        os << std::endl;
      }
    }
  }

  return os;
};

/* Input functions. */
std::istream& operator>>(std::istream &is, State &state)
{
  int row, col, player;
  std::string inputType, junk;

  /* Read in input type. */
  while(is >> inputType) {
    if(inputType == "end") {
      state.gameOver = true;
      break;
    } else if(inputType == "turn") {
      is >> state.currentTurnNumber;
      break;
    } else {
      getline(is, junk);
    }
  }

  if(state.currentTurnNumber == 0) {
    /* Set the ants to zero (the first ant is *not* newborn below. */
    state.antsInHive = 0;

    /* If we are at the beginning of the game, read in the parameters. */
    while(is >> inputType) {
      if(inputType == "loadtime") {
        is >> gparam::loadTime;
      } else if(inputType == "turntime") {
        is >> gparam::turnTime;
      } else if(inputType == "rows") {
        is >> gparam::mapRows;
      } else if(inputType == "cols") {
        is >> gparam::mapColumns;
      } else if(inputType == "turns") {
        is >> gparam::totalTurnsNumber;
      } else if(inputType == "player_seed") {
        is >> gparam::seed;
        srand(gparam::seed);
      } else if(inputType == "viewradius2") {
        is >> gparam::viewRadius;
      } else if(inputType == "attackradius2") {
        is >> gparam::attackRadius;
      } else if(inputType == "spawnradius2") {
        is >> gparam::spawnRadius;
      } else if(inputType == "ready") {
        /* This is the end of the parameter input. */
        state.timer.start();
        break;
      } else {
        getline(is, junk);
      }
    }
  } else {
    /* Reads in information about the current turn. */
    while(is >> inputType) {
      if(inputType == "w") {
        /* Water square. */
        is >> row >> col;
        state.grid[row][col].isWater = 1;
      } else if(inputType == "f") {
        /* Food square. */
        is >> row >> col;
        state.grid[row][col].isFood = 1;
        state.food.push_back(LOCATION(row, col));
      } else if(inputType == "a") {
        /* Live ant square. */
        is >> row >> col >> player;
        state.grid[row][col].ant = player;
        if(player == 0) {
          state.inputMyAnts.push_back(LOCATION(row, col));
        } else {
          state.enemyAnts.push_back(LOCATION(row, col));
        }
      } else if(inputType == "d") {
        /* Dead ant squares. */
        is >> row >> col >> player;
        state.grid[row][col].deadAnts.push_back(player);
      } else if(inputType == "h") {
        /* Hill square. */
        is >> row >> col >> player;
        state.grid[row][col].isHill = 1;
        state.grid[row][col].hillPlayer = player;
        if(player == 0)
          state.inputMyHills.push_back(LOCATION(row, col));
        else
          state.inputEnemyHills.push_back(LOCATION(row, col));
      } else if(inputType == "players") {
        /* Information about the players. */
        is >> gparam::numberPlayers;
      } else if(inputType == "scores") {
        /* Information about the scores. */
        state.scores = std::vector<double>(gparam::numberPlayers, 0.0);
        for(int p = 0; p < gparam::numberPlayers; p++)
          is >> state.scores[p];
      } else if(inputType == "go") {
        /* Finished input. */
        if(state.gameOver) {
          LOG(LOG_INF, "Received end of game message.");
          is.setstate(std::ios::failbit);
        } else {
          state.timer.start();
        }
        break;
      } else {
        getline(is, junk);
      }
    }

    /* Consistency check - the grid should not have any antIndex here. */
    for (unsigned int i = 0; i < gparam::mapRows; ++i) {
      for (unsigned int j = 0; j < gparam::mapColumns; ++j) {
        if (state.grid[i][j].antIndex != -1) {
              LOG(LOG_ERR, "Ant Index should NOT be present here!");
        }
      }
    }

    // Removed persistency in the last minute.
    state.myAnts.clear();
    for (int i = 0; i < state.inputMyAnts.size(); ++i) {
      struct AntInfo info(state.inputMyAnts[i],
                          state.inputMyAnts[i]);
      state.myAnts.push_back(info);

      state.grid[GET_ROW(info.position)][GET_COLUMN(info.position)].antIndex = i;
    }
  }

  return is;
}
