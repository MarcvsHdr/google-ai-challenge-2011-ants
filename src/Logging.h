#ifndef LOGGING_H_
#define LOGGING_H_

#include "gparam.h"

/** Logging tags. Use this to filter out output when compiling so that
  * you don't end up showing whole megabytes of logs...
  */
enum LogLevel {
  LOG_INF = 1 << 0,
  LOG_ERR = 1 << 1,
  LOG_TEST = 1 << 2
  /* Add up to 14 more types, depending on your needs. */
};

/** Change this mask to enable/disable some logging. */
const int LogMask = LOG_INF | LOG_ERR | LOG_TEST;

#ifdef DEBUG

/** Logs message to standard error file. We can change this in the future. */
#define LOG(level, message) \
  (level & LogMask) ? \
    (gparam::logFile << message << std::endl).flush() : \
    gparam::logFile;

#else

/** Disable message logging. */
#define LOG(level, message) do { } while (false)

#endif

#endif
