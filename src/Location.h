#ifndef LOCATION_H_
#define LOCATION_H_

#include <stdint.h>

#include "gparam.h"

/** Locations on the map are represented as 32-bit integers with the format:
  * 0xLLLLCCCC, where:
  *    LLLL represents the line on 16 bits
  *    CCCC represents the column on 16 bits
  */

/** No need to enforce type consistency through templates. The compiler does
  * that for POD types automatically.
  */
typedef uint32_t Location;

/** Constructor. */
#define LOCATION(row, column) (((row) << 16) | (column))

/** Masks. Don't use these outside this file, I mean it! */
#define LOCATION_ROW_MASK 0xFFFF0000
#define LOCATION_COLUMN_MASK 0x0000FFFF

/** Accessors. Only use them if you KNOW what you're doing. */
#define GET_ROW(location) (((location) & LOCATION_ROW_MASK) >> 16)
#define GET_COLUMN(location) ((location) & LOCATION_COLUMN_MASK)

/** Incrementors. They wrap around the torus, use them with confidence. */
#define MOVE_NORTH(location) ( \
  ((location) & LOCATION_ROW_MASK) ? \
    ((location) - (1 << 16)) : \
    ((location) | ((gparam::mapRows - 1) << 16)) \
) 

#define MOVE_SOUTH(location) ( \
  ((((location) & LOCATION_ROW_MASK) >> 16) == (gparam::mapRows - 1)) ? \
    ((location) & LOCATION_COLUMN_MASK) : \
    ((location) + (1 << 16)) \
)

#define MOVE_WEST(location) ( \
  ((location) & LOCATION_COLUMN_MASK) ? \
    ((location) - 1) : \
    ((location) | (gparam::mapColumns - 1)) \
)

#define MOVE_EAST(location) ( \
  (((location) & LOCATION_COLUMN_MASK) == (gparam::mapColumns - 1)) ? \
    ((location) & LOCATION_ROW_MASK) : \
    ((location) + 1) \
)

/** Directional incrementor. Works sort of the same, but takes direction */
#define MOVE_DIRECTION(location, direction) ( \
  ((direction) == NORTH) ? (MOVE_NORTH(location)) : \
    ((direction) == EAST) ? (MOVE_EAST(location)) : \
      ((direction) == SOUTH) ? (MOVE_SOUTH(location)) : \
        ((direction) == WEST) ? (MOVE_WEST(location)) : \
          (location)\
)

/** Simple utils for dealing with otherwise ambiguous mathematical functions.
  * It's best to use these instead of the functions in cmath, because these ones
  * are not type-ambigous and you risk fewer linker errors which you might get
  * if you go bersek on including.
  */
#define ABS(a) ((a) >= 0 ? (a) : -(a))
#define MIN(a, b) ((a) < (b) ? (a) : (b))
#define MAX(a, b) ((a) > (b) ? (a) : (b))

#endif

