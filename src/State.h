#ifndef STATE_H_
#define STATE_H_

#include <iostream>
#include <stdio.h>
#include <cstdlib>
#include <cmath>
#include <string>
#include <vector>
#include <queue>
#include <stack>
#include <map>
#include <stdint.h>

#include <algorithm>

#include "gparam.h"
#include "constants.h"

#include "Logging.h"
#include "Timer.h"
#include "Square.h"
#include "Location.h"

/** Easy access method for a grid square. */
#define GRID_SQUARE(location) grid[GET_ROW(location)][GET_COLUMN(location)]

struct AntInfo {
  Location position;
  Location nextPosition;

  bool moved;
  bool forced;

  AntInfo(Location position, Location nextPosition) {
    this->position = position;
    this->nextPosition = nextPosition;

    this->moved = false;
    this->forced = false;
  }
};

struct State {
  /** Pray we see this number get large. */
  int currentTurnNumber;

  /** Score for each of the current players. */
  std::vector<double> scores;

  /** False while we keep playing. */
  bool gameOver;

  /** See definition of Square for further details. */
  std::vector<std::vector<Square> > grid;

  /** Ant positions... horrible representation...
    * I'm thinking about how to do this better.
    */
  std::vector<AntInfo> myAnts;

  std::vector<Location> inputMyAnts, enemyAnts;
  std::vector<Location> inputMyHills, myHills;
  std::vector<Location> inputEnemyHills, enemyHills;
  std::vector<Location> previousFood, food;

  int antsInHive;

  /** This could have been global, but there you go... */
  Timer timer;

  /** Constructor and destructor don't do anything fancy. */
  State() :
      gameOver(false),
      currentTurnNumber(0)
  {
    for (int i = 0; i < MAXIMUM_MAP_SIZE; ++i) {
      grid.push_back(std::vector<Square>(MAXIMUM_MAP_SIZE, Square()));
    }
  }

  ~State() { }

  /** Clears non-persistent informatin from the grid after a step. */
  void reset();

  /** Outputs move information correctly to the engine. */
  void makeMove(const Location loc, int direction);


  /** This is just square of Euclid distance. */
  double distance(const Location loc1, const Location loc2);

  /** It's ugly, but this avoids a LOT of useless stack references. */
  Location BFSstart;
  Location BFSend;
  std::vector<Location> BFSlocations;

  int cutoffDistance;
  bool killBreadthFirstSearch;

  /** This is a generic BFS function that uses the grid... DON'T yell at me,
    * templates _ALLOW INLINING_ at compile-time, pointers to functions don't.
    * This will be fast, I promise!!
    */
  template<
    bool (State::*predicate)(Location childLocation),
    void (State::*changeState)(Location parentLocation, Location childLocation)
  >
  void breadthFirstSearch(const Location start, const Location end) {
    /* Build a vector and call that interface. */
    breadthFirstSearch<predicate, changeState>(
        std::vector<Location>(1, start), end);
  }

  template<
    bool (State::*predicate)(Location childLocation),
    void (State::*changeState)(Location parentLocation, Location childLocation)
  >
  void breadthFirstSearch(
      const std::vector<Location>& start,
      const Location end) {
    /* Cache the end parameter inside the class. */
    BFSstart = (start.size() == 1) ? start[0] : NOWHERE;
    BFSend = end;
    killBreadthFirstSearch = false;

    /* We use std::vector<bool> because it implements bitwise packing. */
    std::vector<std::vector<bool> > visited(
        gparam::mapRows,
        std::vector<bool>(gparam::mapColumns, false));

    /* Create a queue that contains the start positions. */
    std::queue<Location> locationQueue;
    for (int i = 0; i < start.size(); ++i) {
      locationQueue.push(start[i]);
      /* Do something... if you need to. */
      (this->*changeState)(NOWHERE, start[i]);
      visited[GET_ROW(start[i])][GET_COLUMN(start[i])] = true;
    }

    /* The loop of the exploration. */
    Location currentLocation, newLocation;
    while (!locationQueue.empty() && !killBreadthFirstSearch) {
      /* Explore the first node of the location queue. */
      currentLocation = locationQueue.front();
      locationQueue.pop();

      /* If you were searching for a destination you found, then stop. */
      if (end != NOWHERE && currentLocation == end) {
        break;
      }

      for (int direction = 0; direction < NUMBER_DIRECTIONS; ++direction) {
        newLocation = MOVE_DIRECTION(currentLocation, direction);

        if (visited[GET_ROW(newLocation)][GET_COLUMN(newLocation)] == false &&
            (this->*predicate)(newLocation) == true) {
            locationQueue.push(newLocation);
            /* Do something... if you need to. */
            (this->*changeState)(currentLocation, newLocation);

        }

        /* Make sure you never visit it again. */
        visited[GET_ROW(newLocation)][GET_COLUMN(newLocation)] = true;
      }
    }
  }

  /** Predicates needed for computing various scores. */
  inline bool isAccessible(Location childLocation) {
    return GRID_SQUARE(childLocation).isAccessible;
  }

  inline bool isAccessibleOrNeverSeen(Location childLocation) {
    return GRID_SQUARE(childLocation).isAccessible
      || (!GRID_SQUARE(childLocation).isWater &&
          !GRID_SQUARE(childLocation).isAccessible);
  }

  inline bool isVisible(Location childLocation) {
    return distance(BFSstart, childLocation) <= gparam::viewRadius;
  }

  inline bool isGatherable(Location childLocation) {
    return distance(BFSstart, childLocation) <= gparam::spawnRadius;
  }

  inline bool isOwnAnt(Location childLocation) {
    return GRID_SQUARE(childLocation).ant == 0;
  }

  inline bool isOwnAntOrBetweenDiagonalAnts(Location childLocation) {
    if (isOwnAnt(childLocation)) {
      return true;
    }

    int currentAnt = BFSlocations.size() - 1;
    Location BFSant = BFSlocations[currentAnt];

    while (currentAnt >= 0) {
      BFSant = BFSlocations[currentAnt];

      // Check diagonal BFSant @ (NORTH, SOUTH) & ownAnt @ (EAST, WEST)
      if ((MOVE_SOUTH(childLocation) == BFSant ||
           MOVE_NORTH(childLocation) == BFSant) &&
          (isOwnAnt(MOVE_EAST(childLocation)) ||
           isOwnAnt(MOVE_WEST(childLocation))) ) {
        return true;
      }

      // Check diagonal BFSant @ (EAST, WEST) & ownAnt @ (NORTH, SOUTH)
      if ((MOVE_EAST(childLocation) == BFSant ||
           MOVE_WEST(childLocation) == BFSant) &&
          (isOwnAnt(MOVE_NORTH(childLocation)) ||
           isOwnAnt(MOVE_SOUTH(childLocation)) )) {
        return true;
      }

      currentAnt--;
    }

    return false;
  }

  inline bool isEnemyAnt(Location childLocation) {
    return GRID_SQUARE(childLocation).ant > 0;
  }

  inline bool isAttackable(Location childLocation) {
    return distance(BFSstart, childLocation) <= gparam::attackRadius;
  }

  inline bool isAttackableNextTurn(Location childLocation) {
    if (isAttackable(childLocation))
      return true;
    for (int dir = 0; dir < NUMBER_DIRECTIONS; ++dir) {
      Location antLocation = MOVE_DIRECTION(BFSstart, dir);
      if (distance(antLocation, childLocation) <= gparam::attackRadius) {
        return true;
      }
    }
    return false;
  }

  inline void markVisited(
      Location parentLocation,
      Location childLocation) {
    BFSlocations.push_back(childLocation);
  }
  
  inline void markAntsAroundFood(
      Location parentLocation,
      Location childLocation) {
    if (parentLocation != NOWHERE) {
      if (GRID_SQUARE(childLocation).ant != -1)
        if (GRID_SQUARE(childLocation).ant != 0) {
          // There is an enemy ant within the spawn radius of the food.
          GRID_SQUARE(BFSstart).previousFoodGathered = -1;
        } else {
          // Our ant is there.
          if(GRID_SQUARE(BFSstart).previousFoodGathered == 0) {
            GRID_SQUARE(BFSstart).previousFoodGathered = 1;
          }
        }
    }
  }

  /** Predicates needed for updating the vision information. */
  inline void markVisibility(
      Location parentLocation,
      Location childLocation) {
    GRID_SQUARE(childLocation).isVisible = true;
    GRID_SQUARE(childLocation).isAccessible =
        !GRID_SQUARE(childLocation).isWater;
  }

  inline void markOwnClosest(
      Location parentLocation,
      Location childLocation) {
    if (parentLocation != NOWHERE) {
        GRID_SQUARE(childLocation).ownClosest =
          GRID_SQUARE(parentLocation).ownClosest + 1;
    } else {
        GRID_SQUARE(childLocation).ownClosest = 0;
    }
  }
  
  inline void markOwnClosestFeederCandidate(
      Location parentLocation,
      Location childLocation) {
    if (parentLocation != NOWHERE) {
        GRID_SQUARE(childLocation).ownClosestFeederCandidate =
          GRID_SQUARE(parentLocation).ownClosestFeederCandidate + 1;
    } else {
        GRID_SQUARE(childLocation).ownClosestFeederCandidate = 0;
    }
  }

  inline void markEnemyVisibility(
      Location parentLocation,
      Location childLocation) {
    GRID_SQUARE(childLocation).isEnemyVisible = true;
  }

  inline void markEnemyClosest(
      Location parentLocation,
      Location childLocation) {
    if (parentLocation != NOWHERE) {
        GRID_SQUARE(childLocation).enemyClosest =
          GRID_SQUARE(parentLocation).enemyClosest + 1;
    } else {
        GRID_SQUARE(childLocation).enemyClosest = 0;
    }
  }

  inline void markEnemyHillProximity(
      Location parentLocation,
      Location childLocation) {
    if (parentLocation != NOWHERE) {
        GRID_SQUARE(childLocation).enemyHillProximity =
          GRID_SQUARE(parentLocation).enemyHillProximity + 1;
    } else {
        GRID_SQUARE(childLocation).enemyHillProximity = 0;
    }
  }

  inline void markMyHillProximity(
      Location parentLocation,
      Location childLocation) {
    if (parentLocation != NOWHERE) {
      GRID_SQUARE(childLocation).myHillProximity =
          GRID_SQUARE(parentLocation).myHillProximity + 1;
      GRID_SQUARE(childLocation).closestOwnHill = 
          GRID_SQUARE(parentLocation).closestOwnHill;
    } else {
      GRID_SQUARE(childLocation).myHillProximity = 0;
      for (unsigned int i = 0; i < myHills.size(); i++) {
        if (childLocation == myHills[i])
          GRID_SQUARE(childLocation).closestOwnHill = i;
      }
    }
  }

  /* Predicates for computing danger, blow and defense. */
  inline void markDanger(
      Location parentLocation,
      Location childLocation) {
    if(distance(childLocation, BFSstart) <= gparam::attackRadius) {
      GRID_SQUARE(childLocation).danger[0]++;
      GRID_SQUARE(childLocation).danger[1]++;
      if (GRID_SQUARE(childLocation).blow > GRID_SQUARE(BFSstart).defense[1]) {
        GRID_SQUARE(childLocation).blow = GRID_SQUARE(BFSstart).defense[1];
      }
      if (GRID_SQUARE(childLocation).largestAttackingGroup <
          GRID_SQUARE(BFSstart).groupSize) {
        GRID_SQUARE(childLocation).largestAttackingGroup =
            GRID_SQUARE(BFSstart).groupSize;
      }
    } else {
      for (int dir = 0; dir < NUMBER_DIRECTIONS; ++dir) {
        Location newEnemyLocation = MOVE_DIRECTION(BFSstart, dir);
        if (!GRID_SQUARE(newEnemyLocation).isWater) {
          if (distance(newEnemyLocation, childLocation) <=
              gparam::attackRadius) {
            GRID_SQUARE(childLocation).danger[1]++;
            break;
          }
        }
      }
    }

    for (int dir = 0; dir < NUMBER_DIRECTIONS; ++dir) {
      Location newEnemyLocation = MOVE_DIRECTION(BFSstart, dir);
      if (!GRID_SQUARE(newEnemyLocation).isWater) {
        if (distance(newEnemyLocation, childLocation) <=
            gparam::attackRadius) {
          if (GRID_SQUARE(childLocation).blow >
              GRID_SQUARE(newEnemyLocation).defense[1]) {
            GRID_SQUARE(childLocation).blow =
                GRID_SQUARE(newEnemyLocation).defense[1];
          }
          if (GRID_SQUARE(childLocation).largestAttackingGroup <
              GRID_SQUARE(BFSstart).groupSize) {
            GRID_SQUARE(childLocation).largestAttackingGroup =
                GRID_SQUARE(BFSstart).groupSize;
          }
        }
      }
    }
  }

  inline void markDefense(
      Location parentLocation,
      Location childLocation) {
    if (distance(childLocation, BFSstart) <= gparam::attackRadius) {
      GRID_SQUARE(childLocation).defense[0]++;
      GRID_SQUARE(childLocation).defense[1]++;
    } else {
      for (int dir = 0; dir < NUMBER_DIRECTIONS; ++dir) {
        Location newOwnLocation = MOVE_DIRECTION(BFSstart, dir);
        if(!GRID_SQUARE(newOwnLocation).isWater) {
          if (distance(newOwnLocation, childLocation) <= gparam::attackRadius) {
            GRID_SQUARE(childLocation).defense[1]++;
            break;
          }
        }
      }
    }
  }

  inline void markFrontierProximity(
      Location parentLocation,
      Location childLocation) {
    if (parentLocation != NOWHERE) {
      GRID_SQUARE(childLocation).frontierProximity =
          GRID_SQUARE(parentLocation).frontierProximity + 1;
    } else {
      GRID_SQUARE(childLocation).frontierProximity = 0;
    }
  }

  inline void markVirginFrontierProximity(
      Location parentLocation,
      Location childLocation) {
    if (parentLocation != NOWHERE) {
      GRID_SQUARE(childLocation).virginFrontierProximity =
          GRID_SQUARE(parentLocation).virginFrontierProximity + 1;
    } else {
      GRID_SQUARE(childLocation).virginFrontierProximity = 0;
    }
  }
  
  inline void markClosestGather(
      Location parentLocation,
      Location childLocation) {
    if (parentLocation != NOWHERE) {
      GRID_SQUARE(childLocation).closestGather =
          GRID_SQUARE(parentLocation).closestGather + 1;
    } else {
      GRID_SQUARE(childLocation).closestGather = 0;
    }
  }

  inline void markAsHillAttacker(
      Location parentLocation,
      Location childLocation) {
   /* Write down temp distance. */
    if (parentLocation != NOWHERE) {
      GRID_SQUARE(childLocation).tempDistance =
          GRID_SQUARE(parentLocation).tempDistance + 1;
    } else {
      GRID_SQUARE(childLocation).tempDistance = 0;
    }

    /* And if you've reached a first friendly ant, make it an attacker. */
    if (GRID_SQUARE(childLocation).ant == 0 &&
        GRID_SQUARE(childLocation).role == NOROLE &&
	      BFSend == NOWHERE) {
      /* Write down that now we have a destination. */
      GRID_SQUARE(childLocation).role = ATTACKER;
      GRID_SQUARE(childLocation).attackedHillLocation = BFSstart;
      BFSend = childLocation;
      cutoffDistance = GRID_SQUARE(childLocation).tempDistance + 1;
    } else if (BFSend != NOWHERE &&
               GRID_SQUARE(childLocation).tempDistance > cutoffDistance) {
      /* Kill the BFS if you've found an ant and moved two squares past it. */
      killBreadthFirstSearch = true;
    }
  }

  inline void markAsFeeder(
      Location parentLocation,
      Location childLocation) {
    /* Write down temp distance. */
    if (parentLocation != NOWHERE) {
      GRID_SQUARE(childLocation).tempDistance =
          GRID_SQUARE(parentLocation).tempDistance + 1;
    } else {
      GRID_SQUARE(childLocation).tempDistance = 0;
    }

    /* And if you've reached a first friendly ant, make it an attacker. */
    if (GRID_SQUARE(childLocation).ant == 0 &&
        GRID_SQUARE(childLocation).role == NOROLE &&
	      BFSend == NOWHERE) {
      /* Write down that now we have a destination. */
      GRID_SQUARE(childLocation).role = FEEDER;
      GRID_SQUARE(childLocation).feedingLocation = BFSstart;
      BFSend = childLocation;
      cutoffDistance = GRID_SQUARE(childLocation).tempDistance + 1;
    } else if (BFSend != NOWHERE &&
               GRID_SQUARE(childLocation).tempDistance > cutoffDistance) {
      /* Kill the BFS if you've found an ant and moved two squares past it. */
      killBreadthFirstSearch = true;
    } else if (BFSend == NOWHERE &&
               GRID_SQUARE(childLocation).tempDistance > 10) {
      /* Kill the BFS if you've exceeded 8 and haven't found a friendly ant. */
      killBreadthFirstSearch = true;
    }
  }

  inline void markAsAntAttacker(
      Location parentLocation,
      Location childLocation) {
    /* Write down temp distance. */
    if (parentLocation != NOWHERE) {
      GRID_SQUARE(childLocation).tempDistance =
          GRID_SQUARE(parentLocation).tempDistance + 1;
    } else {
      GRID_SQUARE(childLocation).tempDistance = 0;
    }

    /* And if you've reached a first friendly ant, make it an attacker. */
    if (GRID_SQUARE(childLocation).ant == 0 &&
        GRID_SQUARE(childLocation).role == NOROLE &&
	      BFSend == NOWHERE) {
      /* Write down that now we have a destination. */
      GRID_SQUARE(childLocation).role = ANT_ATTACKER;
      GRID_SQUARE(childLocation).attackedAntLocation = BFSstart;
      BFSend = childLocation;
      cutoffDistance = GRID_SQUARE(childLocation).tempDistance + 1;
    } else if (BFSend != NOWHERE &&
               GRID_SQUARE(childLocation).tempDistance > cutoffDistance) {
      /* Kill the BFS if you've found an ant and moved two squares past it. */
      killBreadthFirstSearch = true;
    } else if (BFSend == NOWHERE &&
               GRID_SQUARE(childLocation).tempDistance > 8) {
      /* Kill the BFS if you've exceeded 8 and haven't found a friendly ant. */
      killBreadthFirstSearch = true;
    }
  }
  
  inline void markAsHillDefender(
      Location parentLocation,
      Location childLocation) {
    /* Write down temp distance. */
    if (parentLocation != NOWHERE) {
      GRID_SQUARE(childLocation).tempDistance =
          GRID_SQUARE(parentLocation).tempDistance + 1;
    } else {
      GRID_SQUARE(childLocation).tempDistance = 0;
    }

    /* And if you've reached a first friendly ant, make it a hill defender. */
    if (GRID_SQUARE(childLocation).ant == 0 &&      
        GRID_SQUARE(childLocation).role == NOROLE &&
        GRID_SQUARE(childLocation).myHillProximity <=
            GRID_SQUARE(BFSstart).myHillProximity &&
	      BFSend == NOWHERE) {
      /* Write down that now we have a destination. */
      GRID_SQUARE(childLocation).role = HILL_DEFENDER;
      GRID_SQUARE(childLocation).defendedAntLocation = BFSstart;
      BFSend = childLocation;
      cutoffDistance = GRID_SQUARE(childLocation).tempDistance + 1;
    } else if (BFSend != NOWHERE &&
               GRID_SQUARE(childLocation).tempDistance > cutoffDistance) {
      /* Kill the BFS if you've found an ant and moved two squares past it. */
      killBreadthFirstSearch = true;
    }
  }
  
   inline void markAsHillDefenderExtra(
      Location parentLocation,
      Location childLocation) {
    /* Write down temp distance. */
    if (parentLocation != NOWHERE) {
      GRID_SQUARE(childLocation).tempDistance =
          GRID_SQUARE(parentLocation).tempDistance + 1;
    } else {
      GRID_SQUARE(childLocation).tempDistance = 0;
    }

    /* And if you've reached a first friendly ant, make it a hill defender. */
    if (GRID_SQUARE(childLocation).ant == 0 &&      
        GRID_SQUARE(childLocation).role == NOROLE &&
	      BFSend == NOWHERE) {
      /* Write down that now we have a destination. */
      GRID_SQUARE(childLocation).role = HILL_DEFENDER;
      GRID_SQUARE(childLocation).defendedAntLocation = BFSstart;
      BFSend = childLocation;
      cutoffDistance = GRID_SQUARE(childLocation).tempDistance + 1;
    } else if (BFSend != NOWHERE &&
               GRID_SQUARE(childLocation).tempDistance > cutoffDistance) {
      /* Kill the BFS if you've found an ant and moved two squares past it. */
      killBreadthFirstSearch = true;
    }
  }

  inline void markAntAttackerProximity(
      Location parentLocation,
      Location childLocation) {
    if (parentLocation != NOWHERE) {
      GRID_SQUARE(childLocation).attackerProximity =
          GRID_SQUARE(parentLocation).attackerProximity + 1;
      GRID_SQUARE(childLocation).attackedAntLocation =
          GRID_SQUARE(parentLocation).attackedAntLocation;
    } else {
      GRID_SQUARE(childLocation).attackerProximity = 0;
    }
  }

  inline void markHillAttackerProximity(
      Location parentLocation,
      Location childLocation) {
    if (parentLocation != NOWHERE) {
      GRID_SQUARE(childLocation).attackerProximity =
          GRID_SQUARE(parentLocation).attackerProximity + 1;
      GRID_SQUARE(childLocation).attackedHillLocation =
          GRID_SQUARE(parentLocation).attackedHillLocation;
    } else {
      GRID_SQUARE(childLocation).attackerProximity = 0;
    }
  }
  
  inline void markTempDistance(
      Location parentLocation,
      Location childLocation) {
    if (parentLocation != NOWHERE) {
      GRID_SQUARE(childLocation).tempDistance =
          GRID_SQUARE(parentLocation).tempDistance + 1;
    } else {
      GRID_SQUARE(childLocation).tempDistance = 0;
    }
  }

  inline void computeDistance(
      Location parentLocation,
      Location childLocation) {
    if (parentLocation != NOWHERE) {
      GRID_SQUARE(childLocation).tempDistance =
          GRID_SQUARE(parentLocation).tempDistance + 1;
    } else {
      for (int i = 0; i < gparam::mapRows; i++)
        for (int j = 0; j < gparam::mapColumns; j++)
          grid[i][j].tempDistance = 0;
    }
  }

  inline void updateVisionInformation() {
    /* BFS from every own ant to mark visibility. */
    for (unsigned int i = 0; i < myAnts.size(); ++i) {
      breadthFirstSearch<&State::isVisible, &State::markVisibility>(
          myAnts[i].position, NOWHERE);
    }

    /* BFS from every enemy ant to mark visibility. */
    for (unsigned int i = 0; i < enemyAnts.size(); ++i) {
      breadthFirstSearch<&State::isVisible, &State::markEnemyVisibility>(
          enemyAnts[i], NOWHERE);
    }
  }

  inline void updateDefenseInformation() {
    /* Compute defense, but only for the ants that attack or defend the hill. */
    for (unsigned int i = 0; i < myAnts.size(); ++i) {
      if (GRID_SQUARE(myAnts[i].position).role == ANT_ATTACKER ||
          GRID_SQUARE(myAnts[i].position).role == ATTACKER ||
	  GRID_SQUARE(myAnts[i].position).role == BACKUP_ANT_ANT ||
	  GRID_SQUARE(myAnts[i].position).role == BACKUP_HILL_ANT ||
	  GRID_SQUARE(myAnts[i].position).role == HILL_DEFENDER) {
      	breadthFirstSearch<&State::isAttackableNextTurn, &State::markDefense>(
          	myAnts[i].position, NOWHERE);
      }
    }
  }

  inline void updateDangerInformation() {
    /* Compute danger. */
    for (unsigned int i = 0; i < enemyAnts.size(); ++i) {
      breadthFirstSearch<&State::isAttackableNextTurn, &State::markDanger>(
          enemyAnts[i], NOWHERE);
    }
  }

  inline void updateBackupMoveScores() {
    /* Create a vector of (attacked location, backup ant position). */
    std::vector<std::pair<Location, Location> > backupAnts;
    for (unsigned int i = 0; i < myAnts.size(); ++i) {
      Square& antSquare = GRID_SQUARE(myAnts[i].position);

      if (antSquare.role == BACKUP_ANT_ANT) {
        backupAnts.push_back(std::pair<Location, Location>(
                antSquare.attackedAntLocation, myAnts[i].position));
      }
      
      if (antSquare.role == BACKUP_HILL_ANT) {
        backupAnts.push_back(std::pair<Location, Location>(
                antSquare.attackedHillLocation, myAnts[i].position));
      }
    }

    /* Group the ants by attacked location. */
    std::sort(backupAnts.begin(), backupAnts.end());

    /* Iterate through the vector and fill in the move scores. */
    Role newRole = NOROLE;
    for (unsigned int i = 0; i < backupAnts.size(); ++i) {
      /* If you changed attacked location. */
      if (i == 0 || (i > 0 && backupAnts[i - 1].first != backupAnts[i].first)) {
        /* Compute distances from attacker for all known map. */
        breadthFirstSearch<&State::isAccessible, &State::markTempDistance>(
            backupAnts[i].first, NOWHERE);
        /* Determine the new role of these backup ants. */
        if (GRID_SQUARE(backupAnts[i].first).hillPlayer > 0) {
          newRole = ATTACKER;
        } else {
          newRole = ANT_ATTACKER;
        }
      }
      /* Based on those distances, fill in backupAttackerScore. */
      Location antLocation = backupAnts[i].second;
      Square& antSquare = GRID_SQUARE(antLocation);
      antSquare.role = newRole;
      for (unsigned int dir = 0; dir < NUMBER_DIRECTIONS + 1; ++dir) {
        Location newLocation = MOVE_DIRECTION(antLocation, dir);
        Square& newSquare = GRID_SQUARE(newLocation);

        if (newSquare.isAccessible) {
          antSquare.antAttackingScore[dir] =
              ((double) 1) / (newSquare.tempDistance);
        }
      }
    }
  }

  inline void updateFrontierDistanceInformation() {
    std::vector<Location> frontierLocations;
    std::vector<Location> virginFrontierLocations;
    /* Compute frontierDistance and virginFrontierDistance. */
    for (unsigned int i = 0; i < gparam::mapRows; i++) {
      for (unsigned int j = 0; j < gparam::mapColumns; j++) {
        if (!grid[i][j].isVisible && !grid[i][j].isWater) {
          /* A frontier location is not visible and has an accessible neigh. */
          for (int dir = 0; dir < NUMBER_DIRECTIONS; ++dir) {
            Location newLocation = MOVE_DIRECTION(LOCATION(i,j), dir);
            if (GRID_SQUARE(newLocation).isVisible) {
              frontierLocations.push_back(LOCATION(i,j));
              break;
            }
          }
        }
        if (!grid[i][j].isAccessible && !grid[i][j].isWater) {
          /* A virgin frontier location is not accessible but has an accessible
           * neighbour. */
          for (int dir = 0; dir < NUMBER_DIRECTIONS; ++dir) {
            Location newLocation = MOVE_DIRECTION(LOCATION(i,j), dir);
            if (GRID_SQUARE(newLocation).isAccessible) {
              virginFrontierLocations.push_back(LOCATION(i,j));
              break;
            }
          }
        }
      }
    }
    breadthFirstSearch<&State::isAccessible,
                       &State::markFrontierProximity>(
        frontierLocations, NOWHERE);
    breadthFirstSearch<&State::isAccessible,
                       &State::markVirginFrontierProximity>(
        virginFrontierLocations, NOWHERE);
  }

  inline void updateMyHillProximityInformation() {
    /* Compute myHillProximity.*/
    breadthFirstSearch<&State::isAccessible, &State::markMyHillProximity>(
        myHills, NOWHERE);
  }

  inline void updateEnemyHillsInformation() {
    /* Compute enemyHillProximity.*/
    breadthFirstSearch<
        &State::isAccessibleOrNeverSeen,
        &State::markEnemyHillProximity>(enemyHills, NOWHERE);
  }

  inline void updateClosestGatherInformation() {
    std::vector<Location> foodAreas;
    for (unsigned int i=0; i < food.size(); i++) {
      foodAreas.push_back(food[i]);
      for (int dir = 0; dir < NUMBER_DIRECTIONS; ++dir) {
        Location newLocation = MOVE_DIRECTION(food[i], dir);
        if (GRID_SQUARE(newLocation).isAccessible)
          foodAreas.push_back(newLocation);
      }
    }
    breadthFirstSearch<&State::isAccessible, &State::markClosestGather>(
        foodAreas, NOWHERE);
  }

  /** Update gathered food information to estimate current ants in hive. */
  inline void updateFoodGathered() {
    for (unsigned int i = 0; i < previousFood.size(); ++i) {
      /* Check if food was gathered by someone. */
      if (!GRID_SQUARE(previousFood[i]).isFood) {
        breadthFirstSearch<&State::isGatherable, &State::markAntsAroundFood>(
            previousFood[i], NOWHERE);

        if (GRID_SQUARE(previousFood[i]).previousFoodGathered == 1) {
          antsInHive++;
        }
      }
    }
  }

  inline void updatePersistentOwnHillsInformation() {
    /* Update myHills -- they are persistent. */
    if (myHills.size() == 0) {
      for (int i = 0; i < inputMyHills.size(); ++i) {
        myHills.push_back(inputMyHills[i]);
      }
    } else {
      /* All our *own* hills are visible at least at the beginning.
       * So it means we just need to update the information we have
       * in case the position on the map is visibile and there is
       * no hill on it - and we would expect to see one there. */
      std::vector<Location> newHills;

      for (int i = 0; i < myHills.size(); ++i) {
        if (!GRID_SQUARE(myHills[i]).isVisible ||
            GRID_SQUARE(myHills[i]).isHill) {
          newHills.push_back(myHills[i]);
        }
      }

      myHills = newHills;
    }
  }

  /** Update enemy hills (persistent information). */
  inline void updatePersistentEnemyHillsInformation() {
    /* Update enemyHills -- they are persistent. */
    if (enemyHills.size() == 0) {
      for (int i = 0; i < inputEnemyHills.size(); ++i) {
        enemyHills.push_back(inputEnemyHills[i]);
      }
    } else {
      std::vector<Location> newEnemyHills;
      std::map<Location, int> checkedHills; // This should be cheap(few hills?)

      // Make sure the newly seen hills are added.
      for (int i = 0; i < inputEnemyHills.size(); ++i) {
        newEnemyHills.push_back(inputEnemyHills[i]);
        checkedHills[inputEnemyHills[i]] = 1;
      }

      // Update the old information about hills.
      for (int i = 0; i < enemyHills.size(); ++i) {
        if (checkedHills.find(enemyHills[i]) != checkedHills.end()) {
          continue;
        }

        Square hillSquare = GRID_SQUARE(enemyHills[i]);
        checkedHills[enemyHills[i]] = 1;

        if (hillSquare.isVisible && !hillSquare.isHill) {
          continue;
        }

        /* Assume nothing changed if the hill is not visible anymore. */
        newEnemyHills.push_back(enemyHills[i]);
      }

      enemyHills.clear();
      for (int i = 0; i < newEnemyHills.size(); i++) {
        enemyHills.push_back(newEnemyHills[i]);
      }
    }

    /** Update the grid with all the enemy hills (known and supposed) */
    for (int i = 0; i < enemyHills.size(); i++) {
      GRID_SQUARE(enemyHills[i]).isHill = true;

      if (!GRID_SQUARE(enemyHills[i]).isVisible) {
        GRID_SQUARE(enemyHills[i]).isHill = true;
        GRID_SQUARE(enemyHills[i]).hillPlayer = UNKNOWN_PLAYER;
      }
    }
  }

  inline void updateGroupSizeInformation() {
    /* BFS from each ant with groupSize = 0 */
    for (int i = 0; i < myAnts.size(); ++i) {
      if (GRID_SQUARE(myAnts[i].position).groupSize == 0) {
        BFSlocations.clear();

        breadthFirstSearch<&State::isOwnAntOrBetweenDiagonalAnts, &State::markVisited>(
            myAnts[i].position, NOWHERE);

        int groupSize = 0;
        for (int ant = 0; ant < BFSlocations.size(); ++ant) {
          if (GRID_SQUARE(BFSlocations[ant]).ant == 0) {
            groupSize++;
          }
        }

        for (int ant = 0; ant < BFSlocations.size(); ++ant) {
          if (GRID_SQUARE(BFSlocations[ant]).ant == 0) {
            GRID_SQUARE(BFSlocations[ant]).groupSize = groupSize;
          }
        }
      }
    }

    /* BFS from each _enemy_ ant with groupSize = 0 */
    for (int i = 0; i < enemyAnts.size(); ++i) {
      if (GRID_SQUARE(enemyAnts[i]).groupSize == 0) {
        BFSlocations.clear();

        breadthFirstSearch<&State::isEnemyAnt, &State::markVisited>(
            enemyAnts[i], NOWHERE);

        for (int ant = 0; ant < BFSlocations.size(); ++ant) {
          GRID_SQUARE(BFSlocations[ant]).groupSize = BFSlocations.size();
        }
      }
    }
  }

  inline void updateOwnClosestInformation() {
    /* BFS from every own ant to mark ownClosest. */
    std::vector<Location> myAntsPositions(myAnts.size(), NOWHERE);
    for (unsigned int i = 0; i < myAnts.size(); ++i) {
       myAntsPositions[i] = myAnts[i].position;
    }
    breadthFirstSearch<&State::isAccessible, &State::markOwnClosest>(
        myAntsPositions, NOWHERE);
  }

  inline void updateEnemyClosestInformation() {
    /* BFS from every enemy ant to mark enemyClosest. */
    breadthFirstSearch<&State::isAccessible, &State::markEnemyClosest>(
        enemyAnts, NOWHERE);
  }
};

/** Methods that will aid in dumping the state at a given time. */
std::ostream& operator<<(std::ostream &os, const State &state);
std::istream& operator>>(std::istream &is, State &state);

#endif
