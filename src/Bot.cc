#include <iostream>
#include <vector>
#include <map>
#include <cstdio>
#include <algorithm>

#include "Bot.h"

void Bot::playGame()
{
  /* Reads in game parameters. */
  LOG(LOG_INF, "Reading initial parameters.");
  std::cin >> state;
  endTurn();

  srand(gparam::seed);

  frontierRadius = 7;

  /* In the start game, we shall use these strategies. */
  Strategy moveDefenderStrategy = &Bot::moveByHillDefense;
  Strategy moveAntAttackerStrategy = &Bot::moveByAntAttacking;
  Strategy moveHillAttackerStrategy = &Bot::moveByHillAttacking;
  Strategy moveFeederStrategy = &Bot::moveByCautiousGathering;
  Strategy moveExplorerStrategy = &Bot::moveBySafeExploring;
  Strategy moveDarkExplorerStrategy = &Bot::moveByDarkExploring;

  /* Continues to make moves until game is over. */
  while(std::cin >> state) {
    LOG(LOG_INF, "turn " << state.currentTurnNumber << ":");

    /** Depends on: NONE | BFS from: each of our ants. */
    state.updateVisionInformation();
    /** Update whatever else you may want on the grid.
     * Please keep these defined in the same order you call them here, and
     * please put in the comments:
     *   - any dependencies on each other they may have;
     *   - source for BFS, or "-" if it's not a BFS;
     */

    /** Depends on: VISION | BFS from: all _previous_ food places. */
    state.updateFoodGathered();
    /** Depends on: VISION | Updates own hills to keep them persistent. */
    state.updatePersistentOwnHillsInformation();
    /** Depends on: VISION | Updates enemy hills to make them persistent. */
    state.updatePersistentEnemyHillsInformation();
    /** Depends on: VISION | BFS from: some of each of our and enemy ants. */
    state.updateGroupSizeInformation();
    /** Depends on: VISION | BFS from: all nodes on the frontier. */
    state.updateFrontierDistanceInformation();
    /** Depends on: VISION | BFS from: all own hill locations. */
    state.updateMyHillProximityInformation();
    /** Depends on: VISION | BFS from: all enemy hill locations. */
    state.updateEnemyHillsInformation();
    /** Depends on: VISION | BFS from: all food places. */
    state.updateClosestGatherInformation();
    /** Depends on: VISION | BFS from: all of our ants. */
    state.updateOwnClosestInformation();
    /** Depends on: VISION | BFS from: all of enemy ants. */
    state.updateEnemyClosestInformation();

    /* Assign roles to the ants, based on computed parameters. */
    assignRoles();

    /** Depends on: VISION, role(ANT_ATTACKER, ATTACKER, BACKUP_ANT) |
     *  BFS from: each of those ants. */
    state.updateDefenseInformation();

    /** Depends on: DEFENSE GROUPSIZE| BFS from: each of enemy ants. */
    state.updateDangerInformation();

    /** Depends on: VISION, DANGER, role(BACKUP_ANT) |
     * BFS from: each of eney ants or hills. |
     * Changes ants from BACKUP_ANT to either ANT_ATTACKER or ATTACKER
     */
    state.updateBackupMoveScores();

    /* Use the strategies for fill in preference vectors for movements. */
    (this->*moveDefenderStrategy)();
    (this->*moveAntAttackerStrategy)();
    (this->*moveHillAttackerStrategy)();
    (this->*moveFeederStrategy)();
    (this->*moveExplorerStrategy)();
    (this->*moveDarkExplorerStrategy)();

    /* Please leave this one here. I changed it to print out scores & stuff. */
    LOG(LOG_INF, state);

    /* Operate moves computed by strategies and then end the turn. */
    makeMoves();

    endTurn();
    LOG(LOG_INF, "time taken: " << state.timer.getTime() << "ms" << std::endl);
    LOG(LOG_INF, "ants in hive: " << state.antsInHive << std::endl);
  }
}

void Bot::assignRoles()
{

  /* Emergency strategy in case we have no more living hills. */
  if (state.myHills.size() == 0) {
    assignNoHillRole();
    return;
  }
  /* See how much of the map is in fact accessible. */
  unsigned int accessibleMap = 0;
  unsigned int darknessMap = 0;
  unsigned int visibleMap = 0;
  for (unsigned int i = 0; i < gparam::mapRows; ++i) {
    for (unsigned int j = 0; j < gparam::mapColumns; ++j) {
      if (state.GRID_SQUARE(LOCATION(i,j)).isAccessible) {
        accessibleMap++;
      } else if (!state.GRID_SQUARE(LOCATION(i,j)).isWater) {
        darknessMap++;
      }
      if (state.GRID_SQUARE(LOCATION(i,j)).isVisible) {
        visibleMap++;
      }
    }
  }

  /* We need at least enough explorers to cover the view radius. */
  unsigned int neededFogExplorers = accessibleMap / (gparam::viewRadius * 3);
  unsigned int neededDarkExplorers =
      2 * 3 * sqrt((double)(MIN(accessibleMap, darknessMap) / 3)) / 16;
  unsigned int neededExplorers = neededFogExplorers + neededDarkExplorers;

  /* Start out with a number of unassigned ants. */
  int unassignedAnts = state.myAnts.size();

  /* And assign roles to the ants, in sequence. */
  assignDefenders(unassignedAnts, 1);  
  assignFeeders(unassignedAnts, 0);
  assignDefenders(unassignedAnts, 0);
  assignHillAttackers(unassignedAnts, 0);
  assignAntAttackers(unassignedAnts, neededExplorers);
  assignBackupsToAntAttackers(unassignedAnts, neededExplorers);
  assignBackupsToHillAttackers(unassignedAnts, neededExplorers);
  assignExplorers(unassignedAnts, neededDarkExplorers);
  assignDefaultRole(unassignedAnts, 0, DARK_EXPLORER);


}

void Bot::assignDefenders(int& unassignedAnts, int spareAtLeast) {  
  /* Select only the enemy ants that threaten the hill. */
  std::vector<std::pair<int, Location> > chuckNorrisAnts;
  for (unsigned int i = 0; i < state.enemyAnts.size(); ++i) {
    Location antLocation = state.enemyAnts[i];
    Square& antSquare = state.GRID_SQUARE(antLocation);
    if (antSquare.isAccessible &&
        antSquare.myHillProximity < MAX(12, state.GRID_SQUARE(
                state.myHills[antSquare.closestOwnHill]).ownClosest)) {
      chuckNorrisAnts.push_back(std::pair<int, Location>(
              antSquare.myHillProximity, antLocation));
    }
  }

  /* Sort them ascendingly by myHillProximity and iterate. */
  std::sort(chuckNorrisAnts.begin(), chuckNorrisAnts.end());  
  for (int i = 0 ; i < chuckNorrisAnts.size() 
       && unassignedAnts > spareAtLeast; ++i) {

    /* Try to attach a defender to this hill attacker. */
    state.breadthFirstSearch<
        &State::isAccessible, &State::markAsHillDefender>(
            chuckNorrisAnts[i].second, NOWHERE);
    if (state.BFSend == NOWHERE) {
      state.breadthFirstSearch<
          &State::isAccessible, &State::markAsHillDefenderExtra>(
              chuckNorrisAnts[i].second, NOWHERE);
    }

    /* Now fill in the antAttackingScore if BFS actually hit an ant. */
    if (state.BFSend != NOWHERE) {
      unassignedAnts--;
      for (unsigned int dir = 0; dir < NUMBER_DIRECTIONS + 1; ++dir) {
        Location newLocation = MOVE_DIRECTION(state.BFSend, dir);
        if (state.GRID_SQUARE(newLocation).isAccessible) {
          state.GRID_SQUARE(state.BFSend).antAttackingScore[dir] =
              ((double) 1) / state.GRID_SQUARE(newLocation).tempDistance;
        }
      }
    }
  }  
}

void Bot::assignFeeders(int& unassignedAnts, int spareAtLeast) {
  /* Fill in own closest feeder vector. */
  std::vector<Location> feederCandidates;
  for (unsigned int i = 0; i < state.myAnts.size(); ++i) {
    if (state.GRID_SQUARE(state.myAnts[i].position).role == NOROLE) {
      feederCandidates.push_back(state.myAnts[i].position);
    }
  }
  state.breadthFirstSearch<
      &State::isAccessible, &State::markOwnClosestFeederCandidate>(
          feederCandidates, NOWHERE);

  /* Sort accessible food items based on ownClosestFeederCandidate. */
  std::vector<std::pair<int, Location> > foodCandidates;
  for (unsigned int i = 0; i < state.food.size(); ++i) {
    Location foodLocation = state.food[i];
    Square& foodSquare = state.GRID_SQUARE(foodLocation);

    if (foodSquare.isAccessible) {
      foodCandidates.push_back(std::pair<int, Location>(
              foodSquare.ownClosestFeederCandidate, foodLocation));
    }
  }
  std::sort(foodCandidates.begin(), foodCandidates.end());

  /* Search from each accessible food and mark closest friendly as FEEDER. */
  for (unsigned int i = 0;
       i < foodCandidates.size() && unassignedAnts > spareAtLeast;
       ++i) {
    Location foodLocation = foodCandidates[i].second;
    Square& foodSquare = state.GRID_SQUARE(foodLocation);

    if (foodSquare.isAccessible) {
      /* Look for closest friendly ant who can eat this bit of food. */
      state.breadthFirstSearch<&State::isAccessible, &State::markAsFeeder>(
          foodLocation, NOWHERE);
      /* Now fill in the scores if BFS actually hit an ant. */
      if (state.BFSend != NOWHERE) {
        unassignedAnts--;

        for (unsigned int dir = 0; dir < NUMBER_DIRECTIONS + 1; ++dir) {
          Location newLocation = MOVE_DIRECTION(state.BFSend, dir);
          if (state.GRID_SQUARE(newLocation).isAccessible) {
            state.GRID_SQUARE(state.BFSend).antFeedingScore[dir] =
                ((double) 1) / state.GRID_SQUARE(newLocation).tempDistance;
          }
        }
      }
    }
  }
}

void Bot::assignHillAttackers(int& unassignedAnts, int spareAtLeast) {
  /* Search from each enemy hill; mark closest friendly within a maximum radius
   * as an attacker. Could destroy the explorers repository. */
  for (unsigned int i = 0;
       i < state.enemyHills.size();
       ++i) {
    if (state.GRID_SQUARE(state.enemyHills[i]).isAccessible) {
      state.breadthFirstSearch<
          &State::isAccessible, &State::markAsHillAttacker>(
              state.enemyHills[i], NOWHERE);
      /* If BFS actually hit an ant, mark it as assigned. */
      if (state.BFSend != NOWHERE) {
        unassignedAnts--;
        for (unsigned int dir = 0; dir < NUMBER_DIRECTIONS + 1; ++dir) {
          Location newLocation = MOVE_DIRECTION(state.BFSend, dir);
          if (state.GRID_SQUARE(newLocation).isAccessible) {
            state.GRID_SQUARE(state.BFSend).antAttackingScore[dir] =
                ((double) 1) / state.GRID_SQUARE(newLocation).tempDistance;
          }
        }
      }
    }
  }
}

void Bot::assignAntAttackers(int& unassignedAnts, int spareAtLeast) {
  /* Search from each enemy ant and mark closest friendly as ANT_ATTACKER. */
  std::vector<std::pair<int, Location> > accessibleEnemyAnts;
  for (unsigned int i = 0; i < state.enemyAnts.size(); ++i) {
    Location antLocation = state.enemyAnts[i];
    Square& antSquare = state.GRID_SQUARE(antLocation);

    /* Ignore ants managed by hill defenders. */
    if (antSquare.isAccessible && antSquare.myHillProximity >= MIN (
            gparam::mapRows/5, gparam::mapColumns/5)) {
      accessibleEnemyAnts.push_back(std::pair<int, Location>(
              antSquare.myHillProximity, antLocation));
    }
  }

  /* Sort them ascendingly by myHillProximity and iterate. */
  int assignedAttackers = 0;
  std::sort(accessibleEnemyAnts.begin(), accessibleEnemyAnts.end());
  for (int i = 0;
       i < accessibleEnemyAnts.size() &&
       unassignedAnts > spareAtLeast &&
       assignedAttackers < unassignedAnts - spareAtLeast;
       --i) {
    state.breadthFirstSearch<&State::isAccessible, &State::markAsAntAttacker>(
        accessibleEnemyAnts[i].second, NOWHERE);
    /* Now fill in the scores if BFS actually hit an ant. */
    if (state.BFSend != NOWHERE) {
      unassignedAnts--;
      assignedAttackers++;
      for (unsigned int dir = 0; dir < NUMBER_DIRECTIONS + 1; ++dir) {
        Location newLocation = MOVE_DIRECTION(state.BFSend, dir);
        if (state.GRID_SQUARE(newLocation).isAccessible) {
          state.GRID_SQUARE(state.BFSend).antAttackingScore[dir] =
              ((double) 1) / state.GRID_SQUARE(newLocation).tempDistance;
        }
      }
    }
  }
}

void Bot::assignBackupsToAntAttackers(int& unassignedAnts, int spareAtLeast) {
  /* Review who si an attacker. */
  std::vector<Location> attackers;
  for (unsigned int i = 0; i < state.myAnts.size(); ++i) {
    Location antLocation = state.myAnts[i].position;
    if (state.GRID_SQUARE(antLocation).role == ANT_ATTACKER) {
      attackers.push_back(antLocation);
    }
  }

  /* Backups must be closer than 5 squares to the attackers. */
  if (attackers.size()) {
    state.breadthFirstSearch<
        &State::isAccessible, &State::markAntAttackerProximity>(
            attackers, NOWHERE);
    /* Everyone else who is close to an attacker becomes a potential BACKUP. */
    std::vector<std::pair<int, Location> > potentialBackups;
    for (int i = 0; i < state.myAnts.size(); ++i) {
      Location antLocation = state.myAnts[i].position;
      Square& antSquare = state.GRID_SQUARE(antLocation);

      if (antSquare.role == NOROLE &&
          antSquare.attackerProximity < 5) {
        potentialBackups.push_back(std::pair<int, Location>(
                antSquare.attackerProximity, antLocation));
      }
    }
    /* Sort potential backups ascendingly and assign them. */
    std::sort(potentialBackups.begin(), potentialBackups.end());
    for (int i = 0;
         i < potentialBackups.size() && unassignedAnts > spareAtLeast;
         ++i) {
      state.GRID_SQUARE(potentialBackups[i].second).role = BACKUP_ANT_ANT;
      unassignedAnts--;
    }
  }
}

void Bot::assignBackupsToHillAttackers(int& unassignedAnts, int spareAtLeast) {
  /* Review who si a hill attacker. */
  std::vector<Location> hillAttackers;
  for (unsigned int i = 0; i < state.myAnts.size(); ++i) {
    Location antLocation = state.myAnts[i].position;
    if (state.GRID_SQUARE(antLocation).role == ATTACKER) {
      hillAttackers.push_back(antLocation);
    }
  }

  /* Assign backups to (HILL) ATTACKERS.
   * Backups do not have a distance restriction. */
  if (hillAttackers.size()) {
    state.breadthFirstSearch<
        &State::isAccessible, &State::markHillAttackerProximity>(
            hillAttackers, NOWHERE);
    /* Everyone else who is close to an attacker becomes a potential BACKUP. */
    std::vector<std::pair<int, Location> > potentialBackups;  
    for (int i = 0; i < state.myAnts.size(); ++i) {
      Location antLocation = state.myAnts[i].position;
      Square& antSquare = state.GRID_SQUARE(antLocation);

      if (antSquare.role == NOROLE &&
          antSquare.attackedHillLocation != NOWHERE) {
        potentialBackups.push_back(std::pair<int, Location>(
                antSquare.attackerProximity, antLocation));
      }
    }
    /* Sort potential backups ascendingly and assign them. */
    std::sort(potentialBackups.begin(), potentialBackups.end());
    for (int i = 0;
         i < potentialBackups.size() && unassignedAnts > spareAtLeast;
         ++i) {
      state.GRID_SQUARE(potentialBackups[i].second).role = BACKUP_HILL_ANT;
      unassignedAnts--;
    }
  }
}

void Bot::assignExplorers(int& unassignedAnts, int spareAtLeast)
{
  /* Create a vector of unassigned ants based on virgin frontier. */
  std::vector<std::pair<int, Location> > candidates;
  for (int i = 0; i < state.myAnts.size(); ++i) {
    Location antLocation = state.myAnts[i].position;
    Square& antSquare = state.GRID_SQUARE(antLocation);

    if (antSquare.role == NOROLE) {
      candidates.push_back(std::pair<int, Location>(
              antSquare.virginFrontierProximity, antLocation));
    }
  }

  /* Sort it ascendingly. */
  std::sort(candidates.begin(), candidates.end());

  /* And assign explorers till you have exhausted the pool. */
  for (int i = candidates.size() - 1;
       i >= 0 && unassignedAnts > spareAtLeast;
       --i) {
    state.GRID_SQUARE(candidates[i].second).role = EXPLORER;
    unassignedAnts--;
  }
}

void Bot::assignDefaultRole(int& unassignedAnts,
                            int spareAtLeast,
                            Role defaultRole) {
  /* Everyone who doesn't have a role gets the default role. */
  for (int i = 0;
       i < state.myAnts.size() && unassignedAnts > spareAtLeast;
       ++i) {
    Square& antSquare = state.GRID_SQUARE(state.myAnts[i].position);
    if (antSquare.role == NOROLE) {
      antSquare.role = defaultRole;
      unassignedAnts--;
    }
  }
}

void Bot::assignNoHillRole() {
  for (int i = 0; i < state.myAnts.size(); ++i) {
    Square& antSquare = state.GRID_SQUARE(state.myAnts[i].position);
    if (antSquare.enemyHillProximity < MAXIMUM_ROAD) {
      antSquare.role = ATTACKER;
      for (int dir = 0; dir < NUMBER_DIRECTIONS + 1; ++dir) {
        Location newLocation = MOVE_DIRECTION(state.myAnts[i].position, dir);
        Square& newSquare = state.GRID_SQUARE(newLocation);
        antSquare.antAttackingScore[dir] = newSquare.enemyHillProximity;
      }
    } else {
      antSquare.role = DARK_EXPLORER;
    }
  }  
}

double Bot::getNewSquareScore(const Square &newSquare,
                              const Square &antSquare,
                              double strategyScore) {
  double score;
  if (newSquare.danger[1] > 0 && newSquare.danger[1] < newSquare.blow) {
    score = 30 + strategyScore;
  } else if (newSquare.danger[1] > 0 &&
             newSquare.danger[1] == newSquare.blow &&
             newSquare.largestAttackingGroup < antSquare.groupSize &&
             newSquare.blow > 1) {
    score = 20 + strategyScore;
  } else if (newSquare.danger[1] > 0 &&
             newSquare.danger[1] > newSquare.blow &&
             newSquare.blow>=2 &&
             newSquare.largestAttackingGroup < antSquare.groupSize &&
             antSquare.groupSize > 4) {
    score = 10 + strategyScore;
  } else if (newSquare.danger[1] > 0) {
    score = -10 - newSquare.danger[1] + strategyScore;
  } else {
    score = strategyScore;
  }
  if (newSquare.hillPlayer == 0 && state.antsInHive > 0) {
    score -= 1000;
  }

  return score;
}

void Bot::moveByHillDefense()
{
  for (unsigned int i = 0; i < state.myAnts.size(); ++i) {
    /* For each hill defender, compute scores. */
    Location antLocation = state.myAnts[i].position;
    Square& antSquare = state.GRID_SQUARE(antLocation);

    if (antSquare.role == HILL_DEFENDER) {
      for (int dir = 0; dir < NUMBER_DIRECTIONS + 1; ++dir) {
        Location newLocation = MOVE_DIRECTION(antLocation, dir);
        Square& newSquare = state.GRID_SQUARE(newLocation);
        if (newSquare.isAccessible && !newSquare.isFood) {
          antSquare.moves.push_back(dir);
          double strategyScore =
              (50 * antSquare.antAttackingScore[dir] -
               (((double) 1) / (newSquare.myHillProximity + 1))) / ((double)50);
          antSquare.moveScore[dir] =
              getNewSquareScore(newSquare, antSquare, strategyScore);
          if (newSquare.closestOwnHill ==
              state.GRID_SQUARE(antSquare.defendedAntLocation).closestOwnHill &&
              newSquare.myHillProximity >=
              state.GRID_SQUARE(antSquare.defendedAntLocation).myHillProximity)
            antSquare.moveScore[dir] -= 500;
        }
      }
    }
  }
}

void Bot::moveByAntAttacking()
{
  for (unsigned int i = 0; i < state.myAnts.size(); ++i) {
    /* For each ant that is an attacker, compute scores. */
    Location antLocation = state.myAnts[i].position;
    Square& antSquare = state.GRID_SQUARE(antLocation);

    if (antSquare.role == ANT_ATTACKER) {
      for (int dir = 0; dir < NUMBER_DIRECTIONS + 1; ++dir) {
        Location newLocation = MOVE_DIRECTION(antLocation, dir);
        Square& newSquare = state.GRID_SQUARE(newLocation);

        if (newSquare.isAccessible && !newSquare.isFood) {
          antSquare.moves.push_back(dir);
          double strategyScore = antSquare.antAttackingScore[dir];
          antSquare.moveScore[dir] =
              getNewSquareScore(newSquare, antSquare, strategyScore);
        }
      }
    }
  }
}


/* DANGER: if attackedHillLocation is being used here, rethink
 * assignNoHillRole(). */
void Bot::moveByHillAttacking()
{
  for (unsigned int i = 0; i < state.myAnts.size(); ++i) {
    /* For each ant that is an attacker, compute scores. */
    Location antLocation = state.myAnts[i].position;
    Square& antSquare = state.GRID_SQUARE(antLocation);
    if (antSquare.role == ATTACKER) {

      for (int dir = 0; dir < NUMBER_DIRECTIONS + 1; ++dir) {
        Location newLocation = MOVE_DIRECTION(antLocation, dir);
        Square& newSquare = state.GRID_SQUARE(newLocation);
        if (newSquare.isAccessible && !newSquare.isFood) {
          antSquare.moves.push_back(dir);
          double strategyScore = antSquare.antAttackingScore[dir];
          antSquare.moveScore[dir] =
              getNewSquareScore(newSquare, antSquare, strategyScore);
        }
      }
    }
  }
}

void Bot::moveByCautiousGathering()
{
  for (unsigned int i = 0; i < state.myAnts.size(); ++i) {
    /* For each ant that is a feeder, compute scores. */
    Location antLocation = state.myAnts[i].position;
    Square& antSquare = state.GRID_SQUARE(antLocation);

    if (antSquare.role == FEEDER) {
      /* Feed, but keep an eye out for danger. */
      for (int dir = 0; dir < NUMBER_DIRECTIONS + 1; ++dir) {
        Location newLocation = MOVE_DIRECTION(antLocation, dir);
        Square& newSquare = state.GRID_SQUARE(newLocation);

        if (newSquare.isAccessible && !newSquare.isFood) {
          antSquare.moves.push_back(dir);
          double strategyScore = antSquare.antFeedingScore[dir];

          if (antSquare.antFeedingScore[dir] == 1 && newSquare.danger[1] < 2) {
            strategyScore += 50;
          }

          antSquare.moveScore[dir] =
              getNewSquareScore(newSquare, antSquare, strategyScore);
        }
      }
    }
  }
}

void Bot::moveBySafeExploring()
{
  for (unsigned int i = 0; i < state.myAnts.size(); ++i) {
    /* For each ant that is an explorer, compute scores. */
    Location antLocation = state.myAnts[i].position;
    Square& antSquare = state.GRID_SQUARE(antLocation);

    if (antSquare.role == EXPLORER) {
      /* Explore, but keep an eye out for danger. */
      for (int dir = 0; dir < NUMBER_DIRECTIONS + 1; ++dir) {
        Location newLocation = MOVE_DIRECTION(antLocation, dir);
        Square& newSquare = state.GRID_SQUARE(newLocation);

        if (newSquare.isAccessible && !newSquare.isFood) {
          antSquare.moves.push_back(dir);
          double randomValue = (rand()%1000)/((double)1000);
          LOG(LOG_INF, "!"<<randomValue);
          double strategyScore =
              ((double)MAXIMUM_ROAD +
               1 - newSquare.frontierProximity - randomValue) /
              ((double)MAXIMUM_ROAD);
          antSquare.moveScore[dir] =
              getNewSquareScore(newSquare, antSquare, strategyScore);
        }
      }
    }
  }
}

void Bot::moveByDarkExploring()
{ 
  for (unsigned int i = 0; i < state.myAnts.size(); ++i) {
    /* For each ant that is an explorer, compute scores. */
    Location antLocation = state.myAnts[i].position;
    Square& antSquare = state.GRID_SQUARE(antLocation);

    if (antSquare.role == DARK_EXPLORER) {
      /* Explore, but keep an eye out for danger. */
      for (int dir = 0; dir < NUMBER_DIRECTIONS + 1; ++dir) {
        Location newLocation = MOVE_DIRECTION(antLocation, dir);
        Square& newSquare = state.GRID_SQUARE(newLocation);

        if (newSquare.isAccessible && !newSquare.isFood) {
          antSquare.moves.push_back(dir);
          double randomValue = (rand()%1000)/((double)1000);
          LOG(LOG_INF, "!"<<randomValue);
          double strategyScore =
              ((double)MAXIMUM_ROAD +
               1  - newSquare.virginFrontierProximity - randomValue) /
              ((double)MAXIMUM_ROAD);
          antSquare.moveScore[dir] =
              getNewSquareScore(newSquare, antSquare, strategyScore);
        }
      }
    }
  }
}

void Bot::makeMoves()
{  

  for (unsigned int i = 0; i < state.myAnts.size(); ++i) { 
    Location antLocation = state.myAnts[i].position;
    std::vector<int>& moves = state.GRID_SQUARE(antLocation).moves;
    for (int i = 0; i < state.GRID_SQUARE(antLocation).moves.size(); ++i) {
      for (int j = i + 1;
           j < state.GRID_SQUARE(antLocation).moves.size();
           ++j) {
        if ((state.GRID_SQUARE(antLocation).moveScore[moves[i]] <
             state.GRID_SQUARE(antLocation).moveScore[moves[j]]) ||
            (state.GRID_SQUARE(antLocation).moveScore[moves[i]] ==
             state.GRID_SQUARE(antLocation).moveScore[moves[j]] &&
             state.GRID_SQUARE(
                 MOVE_DIRECTION(antLocation, moves[i])).ant == 0 &&
             state.GRID_SQUARE(
                 MOVE_DIRECTION(antLocation, moves[j])).ant != 0)) {
          std::swap(moves[i], moves[j]);
        }
      }
    }
  }

  for (unsigned int i = 0; i < state.myAnts.size(); ++i) {
    state.myAnts[i].moved = false;
  }
  int movedAnts;
  do {
    /* Nobody is forced to move. */
    for (unsigned int i = 0; i < state.myAnts.size(); ++i) {
      state.myAnts[i].forced = false;
    }

    /* See who is forced. */
    for (unsigned int i = 0; i < state.myAnts.size(); ++i) {
      if (!state.myAnts[i].moved) {
        Location antLocation = state.myAnts[i].position;
        int dir =
            state.GRID_SQUARE(antLocation).moves.size() > 0 ?
            (state.GRID_SQUARE(antLocation).moves[0]) :
            NODIRECTION;
        Location newLocation = MOVE_DIRECTION(antLocation, dir);
        int antIndex = state.GRID_SQUARE(newLocation).antIndex;
        if (dir!=NODIRECTION && antIndex !=-1) {
          state.myAnts[antIndex].forced = true;
        }
      }
    }

    /* Move the unforced ants. */
    bool noneMoved = true;
    for (unsigned int i = 0; i < state.myAnts.size(); ++i) {
      if (!state.myAnts[i].forced && !state.myAnts[i].moved) {
        moveAnt (i, false);
        noneMoved = false;
      }
    }

    /* If all are forced, move one randomly to break the deadlock. */
    if (noneMoved) {
      for (unsigned int i = 0; i < state.myAnts.size(); ++i) {
        if (!state.myAnts[i].moved) {
          moveAnt (i, false);
          break;
        }
      }
    }

    movedAnts = 0;
    for (unsigned int i = 0; i < state.myAnts.size(); ++i) {
      if (state.myAnts[i].moved)
        movedAnts++;
    }
  } while (movedAnts < state.myAnts.size());
  for (int row = 0; row < gparam::mapRows; row++) {
    for (int col = 0; col < gparam::mapColumns; col++) {
      state.grid[row][col].ant = state.grid[row][col].nextTurnAnt;
    }
  }
}

void Bot::moveAnt(unsigned int antIndex, bool forced) {
  Location antLocation = state.myAnts[antIndex].position;

  std::vector<int>& moves = state.GRID_SQUARE(antLocation).moves;
  for (int i = 0; i < state.GRID_SQUARE(antLocation).moves.size(); ++i) {
    for (int j = i + 1;
         j < state.GRID_SQUARE(antLocation).moves.size();
         ++j) {
      if ((state.GRID_SQUARE(antLocation).moveScore[moves[i]] <
           state.GRID_SQUARE(antLocation).moveScore[moves[j]]) ||
          (state.GRID_SQUARE(antLocation).moveScore[moves[i]] ==
           state.GRID_SQUARE(antLocation).moveScore[moves[j]] &&
           state.GRID_SQUARE(
               MOVE_DIRECTION(antLocation, moves[i])).ant == 0 &&
           state.GRID_SQUARE(
               MOVE_DIRECTION(antLocation, moves[j])).ant != 0))  {
        std::swap(moves[i], moves[j]);
      }
    }
  }

  /* Iterate through your desirable locations. */
  bool moved = false;
  for (int i = 0;
       i < state.GRID_SQUARE(antLocation).moves.size() && !moved;
       ++i) {
    int dir = state.GRID_SQUARE(antLocation).moves[i];
    if (dir != NODIRECTION || !forced) {
      Location newLocation = MOVE_DIRECTION(antLocation, dir);
      /* If target location has one of our ants, stay here and move it first. */
      if (dir != NODIRECTION &&
          state.GRID_SQUARE(newLocation).antIndex != -1 &&
          !state.GRID_SQUARE(newLocation).antMovedHere &&
          !state.GRID_SQUARE(newLocation).isMoving &&
          !state.myAnts[state.GRID_SQUARE(newLocation).antIndex].moved) {
        state.GRID_SQUARE(antLocation).isMoving = true;
        moveAnt(state.GRID_SQUARE(newLocation).antIndex, true);
        state.GRID_SQUARE(antLocation).isMoving = false;
      }
      /* If in the end you can move to desired location, do it. */
      if (dir == NODIRECTION || !state.GRID_SQUARE(newLocation).antMovedHere) {
        state.makeMove(antLocation, dir);
        moved = true;
        state.myAnts[antIndex].moved = true;
      }
    }
  }

  /* All desirable locations were taken. */
  if (!moved) {
    state.makeMove(antLocation, NODIRECTION);
    state.myAnts[antIndex].moved = true;
  }
}

void Bot::endTurn()
{
  LOG(LOG_INF, "Sending endTurn()");

  /* If this wasn't the start game, reset the board. */
  if(state.currentTurnNumber > 0) {
    state.reset();
  }

  /* Move to next turn. */
  state.currentTurnNumber++;

  std::cout << "go" << std::endl;
}
