#ifndef BOT_H_
#define BOT_H_

#include "State.h"

struct Bot
{
  /** Current state of the game. */
  State state;

  /** Current frontier radius. */
  int frontierRadius;

  /** Plays a single game of Ants. */
  void playGame();

  /* Assigns roles to ants. */
  void assignDefenders(int& unassignedAnts, int spareAtLeast);
  void assignFeeders(int& unassignedAnts, int spareAtLeast);
  void assignHillAttackers(int& unassignedAnts, int spareAtLeast);
  void assignAntAttackers(int& unassignedAnts, int spareAtLeast);
  void assignBackupsToAntAttackers(int& unassignedAnts, int spareAtLeast);
  void assignBackupsToHillAttackers(int& unassignedAnts, int spareAtLeast);
  void assignExplorers(int& unassignedAnts, int spareAtLeast);
  void assignDefaultRole(int& uassignedAnts,
                         int spareAtLeast,
                         Role defaultRole);
  void assignNoHillRole();
  void assignRoles();

  /** Move by exploring based on fear from meeting enemies. */
  void moveBySafeExploring();

  /** Move by feeding based on fear from meeting enemies. */
  void moveByCautiousGathering();

  /** Move by attacking an enemy ant. */
  void moveByAntAttacking();

  /** Move by attacking an enemy hill. */
  void moveByHillAttacking();

  /** Move by responding to a siege or breaking through it. */
  void moveByHillDefense();

  /** Moves ants by composite formulae. */
  void moveByCautiousExploring();

  /** Move ants by wandering in the dark. */
  void moveByDarkExploring();

  /** Get the new square score.*/
  double getNewSquareScore(const Square &newSquare,
                           const Square &antSquare,
                           double strategyScore);

  /** Operates the moves based on the vector of preferences. */
  void makeMoves();

  /** Called by makeMoves to move a single ant. */
  void moveAnt(unsigned int antIndex, bool forced);

  /** Indicates to the engine that it has made its moves. */
  void endTurn();

  /**  Type of a function that implements a strategy to move ants on the grid
   * at a given step. */
  typedef void (Bot::*Strategy)(void);
};

#endif

