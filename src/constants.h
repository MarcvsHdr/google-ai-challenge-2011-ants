#ifndef CONSTANTS_H_
#define CONSTANTS_H_

#include "Location.h"

/** Location constants. */
const Location NOWHERE = LOCATION(0xFFFF, 0xFFFF);

/** Maximum map size. */
const int MAXIMUM_MAP_SIZE = 200;

#define MAXIMUM_ROAD (MAXIMUM_MAP_SIZE * MAXIMUM_MAP_SIZE)

/* Using something to mark dead positions. */
#define ILLEGAL_POSITION  100000

/* Using something to mark an unknown hill player. */
#define UNKNOWN_PLAYER 1000

/** Directionality constants. */
const int NUMBER_DIRECTIONS = 4;
const int NORTH = 0;
const int EAST = 1;
const int SOUTH = 2;
const int WEST = 3;
const int NODIRECTION = 4;

/** Role constants. */
enum Role {
  HILL_DEFENDER,
  BACKUP_ANT_ANT,
  BACKUP_HILL_ANT,
  ANT_ATTACKER,
  ATTACKER,
  FEEDER,
  EXPLORER,
  DARK_EXPLORER,
  NUMBER_ROLES,
  NOROLE
};

/** Pretty printing aids. */
const char DIRECTION_LETTER[NUMBER_DIRECTIONS+1] = { 'N', 'E', 'S', 'W', 'X' };
const char ROLE_SYMBOL[NUMBER_ROLES] = {
  '@', '#', '*', '~', '!', '$', '^', '&' };

#endif

