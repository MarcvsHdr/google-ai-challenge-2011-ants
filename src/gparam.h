#ifndef GPARAM_H_
#define GPARAM_H_

#include <stdint.h>

#include <iostream>
#include <fstream>

namespace gparam {

/** Number of rows and columns of the map. */
extern int mapRows;
extern int mapColumns;

/** Number of players on the map. */
extern int numberPlayers;

/** Initial seed. */
extern int64_t seed;

/** Number of available turns. */
extern int totalTurnsNumber;

/** Attack constants. */
extern double attackRadius;
extern double spawnRadius;
extern double viewRadius;

/** Time per load and time per turn. */
extern double loadTime;
extern double turnTime;

/** Log file. */
extern std::ofstream logFile;

}

#endif

