The files in this package are part of the C++ starter package for Ants from the
Google AI Challenge.

The Google AI Challenge is an Artificial Intelligence programming contest.
You can get more information by visiting www.ai-contest.com.

The entire contents of this starter package are released under the Apache
license as is all code related to the Google AI Challenge.
See https://github.com/aichallenge/aichallenge/ for more details.

There is a tools package you can download from the contests website, including
an engine, visualizer and some sample bots to test against.

There are also some tutorials on the website for how you can use them.
