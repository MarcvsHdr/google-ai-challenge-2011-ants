CC=g++
CFLAGS=-O3 -funroll-loops -c
LDFLAGS=-lm

SOURCES=$(wildcard src/*.cc)
MAINSOURCES=$(wildcard main_src/*.cc)

OBJECTS=$(addprefix bin/,$(notdir $(SOURCES:.cc=.o)))
MAINOBJECTS=$(addprefix main_bin/,$(notdir $(MAINSOURCES:.cc=.o)))

EXECUTABLEOBJECT=main_bin/MyBot.o
EXECUTABLE=tools/MyBot
ALLEXECUTABLES=$(addprefix main_bin/,$(notdir $(MAINSOURCES:.cc=)))

#Comment/Uncomment the following to enable debugging
ifdef LOGFILENAME
CFLAGS+=-g -DDEBUG -DLOGFILENAME=\"$(LOGFILENAME)\"
else
CFLAGS+=-g -DDEBUG
endif

all: clean $(OBJECTS) $(MAINOBJECTS) $(EXECUTABLE) $(ALLEXECUTABLES)

$(EXECUTABLE): $(OBJECTS)
	$(CC) $(LDFLAGS) $(OBJECTS) $(EXECUTABLEOBJECT) -o $@

unit: $(ALLEXECUTABLES)

main_bin/%: main_bin/%.o $(OBJECTS)
	$(CC) $(LDFLAGS) -o $@ $^

main_bin/%.o: main_src/%.cc
	$(CC) $(CFLAGS) $(LDFLAGS) -Isrc -c -o $@ $^

bin/%.o: src/%.cc
	$(CC) $(CFLAGS) $(LDFLAGS) -c -o $@ $^

clean: 
	-rm -f bin/*.o main_bin/*.o $(EXECUTABLE) $(ALLEXECUTABLES) *.d
	-rm -f debug.txt

.PHONY: all clean

