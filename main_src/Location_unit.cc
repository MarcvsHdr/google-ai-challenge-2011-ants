#include <iostream>

#include "Location.h"

#include "Logging.h"

#define TEST(cond) \
    if ((cond)) { \
      LOG(LOG_TEST, "\tPASS"); \
    } else { \
      LOG(LOG_TEST, "\tFAIL"); \
      failedTests++; \
    }

#define RUN_TEST(testFunction) \
    testFailures = testFunction(); \
    LOG(LOG_TEST, "Failed: " << testFailures); \
    LOG(LOG_TEST, ""); \
    totalFailures += testFailures;

int testFailures;
int totalFailures;

/** This is what a test function should look like. It always returns the number
  * of failures it encountered.
  */
typedef int (*TestFunction) (void);

void startTesting()
{
  LOG(LOG_TEST, "Running unit tests:");
  LOG(LOG_TEST, "===================");
  LOG(LOG_TEST, "");
}

void finishTesting()
{
  LOG(LOG_TEST, "Finished running unit tests.");
  LOG(LOG_TEST, "============================");
  LOG(LOG_TEST, "Total Failures:");
  LOG(LOG_TEST, "\t" << totalFailures);
  LOG(LOG_TEST, "");
}

int testLocation()
{
  int failedTests = 0;
  /* Set map size to be 200. */
  gparam::mapRows = 200;
  gparam::mapColumns = 200;

  LOG(LOG_TEST, "Testing Location.h:");
  LOG(LOG_TEST, "===================");
  
  LOG(LOG_TEST, "Constructor and accessor...");
  TEST(GET_ROW(LOCATION(10, 10)) == 10);
  TEST(GET_ROW(LOCATION(0, 0)) == 0);
  TEST(GET_COLUMN(LOCATION(10, 10)) == 10);
  TEST(GET_COLUMN(LOCATION(0, 0)) == 0);

  LOG(LOG_TEST, "North movements...");
  TEST(GET_ROW(MOVE_NORTH(LOCATION(10, 10))) == 9);
  TEST(GET_ROW(MOVE_NORTH(LOCATION(0, 10))) == gparam::mapRows - 1);
  TEST(GET_COLUMN(MOVE_NORTH(LOCATION(10, 10))) == 10);
  TEST(GET_COLUMN(MOVE_NORTH(LOCATION(0, 10))) == 10);

  LOG(LOG_TEST,"East movements...");
  TEST(GET_ROW(MOVE_EAST(LOCATION(10, 10))) == 10);
  TEST(GET_ROW(MOVE_EAST(LOCATION(10, 199))) == 10);
  TEST(GET_COLUMN(MOVE_EAST(LOCATION(10, 10))) == 11);
  TEST(GET_COLUMN(MOVE_EAST(LOCATION(10, 199))) == 0);

  LOG(LOG_TEST, "South movements...");
  TEST(GET_ROW(MOVE_SOUTH(LOCATION(10, 10))) == 11);
  TEST(GET_ROW(MOVE_SOUTH(LOCATION(199, 10))) == 0);
  TEST(GET_COLUMN(MOVE_SOUTH(LOCATION(10, 10))) == 10);
  TEST(GET_COLUMN(MOVE_SOUTH(LOCATION(199, 10))) == 10);

  LOG(LOG_TEST, "West movements...");
  TEST(GET_ROW(MOVE_WEST(LOCATION(10, 10))) == 10);
  TEST(GET_ROW(MOVE_WEST(LOCATION(10, 0))) == 10);
  TEST(GET_COLUMN(MOVE_WEST(LOCATION(10, 10))) == 9);
  TEST(GET_COLUMN(MOVE_WEST(LOCATION(10, 0))) == 199);

  return failedTests;
}

int main()
{
  startTesting();

  RUN_TEST(testLocation);

  finishTesting();
  return 0;
}

